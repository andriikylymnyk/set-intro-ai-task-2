from logic import *

# Define lists of coaches and teams
coaches = ["Antonio", "Rodrigo", "Mykola"]
teams = ["Milan", "Real", "Metalist"]

# Initialize an empty list to store symbols
symbols = []

# Initialize knowledge base
knowledge = And()

# Create symbols from coach and team combinations
for coach in coaches:
    for team in teams:
        symbols.append(Symbol(f"{coach}{team}"))

# Each coach belongs to a team.
for coach in coaches:
    knowledge.add(Or(
        Symbol(f"{coach}Milan"),
        Symbol(f"{coach}Real"),
        Symbol(f"{coach}Metalist")
    ))

# Only one team per coach.
for coach in coaches:
    for team1 in teams:
        for team2 in teams:
            if team1 != team2:
                knowledge.add(
                    Implication(Symbol(f"{coach}{team1}"), Not(Symbol(f"{coach}{team2}")))
                )

# Only one coach per team.
for team in teams:
    for coach1 in coaches:
        for coach2 in coaches:
            if coach1 != coach2:
                knowledge.add(
                    Implication(Symbol(f"{coach1}{team}"), Not(Symbol(f"{coach2}{team}")))
                )

# Antonio is not the coach of Milan
knowledge.add(Not(Symbol("AntonioMilan")))
# Rodrigo is not the coach of Real.
knowledge.add(Not(Symbol("RodrigoReal")))
# Mykola is not the coach of Metalist.
knowledge.add(Not(Symbol("MykolaMetalist")))
# Antonio is not the coach of Metalist.
knowledge.add(Not(Symbol("AntonioMetalist")))
# Mykola is not the coach of Real.
knowledge.add(Not(Symbol("MykolaReal")))

# Function to check knowledge and show coach of each team
def check_knowledge(knowledge, symbols):
    for symbol in symbols:
        if model_check(knowledge, symbol):
            word = list(symbol.symbols())[0]
            index = next((i for i, c in enumerate(word[1:], 1) if c.isupper()), None)
            coach = word[:index]
            team = word[index:]
            print(f"{coach} is the coach of {team}")

check_knowledge(knowledge, symbols)
